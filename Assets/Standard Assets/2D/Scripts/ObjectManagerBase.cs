﻿using UnityEngine;
using System.Collections;

public class ObjectManagerBase : MonoBehaviour {
    
    protected ArrayList m_AliveArray = new ArrayList();
    protected ArrayList m_DeadArray = new ArrayList();

    public GameObject m_Graveyard;

    public GameObject m_ObjectToStore;
    public int m_NumberOfObjects;

    // Use this for initialization
    void Start ()
    {
        CreateObjects();
        
	}
	

    protected virtual void CreateObjects()
    {
        GameObject newObj = null;

        for (int i = 0; i < m_NumberOfObjects; i++)
        {
            newObj = Instantiate<GameObject>(m_ObjectToStore);

            newObj.transform.SetParent(m_Graveyard.transform);
            newObj.transform.position = m_Graveyard.transform.position;
            newObj.SetActive(false);

            m_DeadArray.Add(newObj);
        }
    }

    protected virtual void UpdateObjects()
    {

    }

    public virtual void KillAllObjects()
    {
        GameObject curObj = null;

        int arrayCount = m_AliveArray.Count;

        for (int i = 0; i < arrayCount; i ++)
        {
            curObj = (GameObject)m_AliveArray[i];

            curObj.SetActive(false);
            curObj.transform.position = m_Graveyard.transform.position;

            m_DeadArray.Add(curObj);
        }

        m_AliveArray.Clear();
    }
}
