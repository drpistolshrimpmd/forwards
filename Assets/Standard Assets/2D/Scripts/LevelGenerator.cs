﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour {

    static int NUM_OF_EACH_FLOOR_TYPE = 5;
    static int CREATE_NEXT_COMBO_CHECK = 4;

    //The block and the amount of blocks to hold and the list to hold them
    public GameObject m_FloorMenu;
    public GameObject m_FloorPlain;
    public GameObject m_FloorSwipe;
    public GameObject m_FloorGap;

    //Make the list size to be x times the size of each floor type. It will hold that many of each.
    protected int m_BlockCount = (int)GameMetadata.NUM_OF_TYPES * NUM_OF_EACH_FLOOR_TYPE;

    private ArrayList m_AliveFloorArray = new ArrayList();
    private ArrayList m_DeadFloorArray = new ArrayList();

    public GameObject m_Graveyard;

    protected Transform m_Player;

    public GameObject m_StartingFloor;
    //Level generator holds onto the current block so that it can get the positions to build from
    //In the editor this is set as the start block that's always there.
    protected GameObject m_CurrentFloorPiece;

	// Use this for initialization
	void Start () {
        m_Player = GameObject.FindGameObjectWithTag("Player").transform;
        m_CurrentFloorPiece = m_StartingFloor;
        SpawnFloorPieces();
    }
	
	// Update is called once per frame
	void Update () {
        StartCoroutine(UpdateFloor());
    }

    IEnumerator UpdateFloor()
    {
        GameObject curFloor;
        FloorBase curFloorBase = null;

        //Check for dead blocks
        for (int i = 0; i < m_AliveFloorArray.Count; i++)
        {
            curFloor = (GameObject)m_AliveFloorArray[i];
            curFloorBase = curFloor.GetComponent<FloorBase>();

            //Check if block is behind the character and off screen
            if (CheckFloorDead(curFloor))
            {
                //If so add it back to dead array
                curFloor.SetActive(false);
                curFloorBase.OnDeath();
                curFloor.transform.position = m_Graveyard.transform.position;

                //At the dead block to the dead block array
                m_DeadFloorArray.Add(curFloor);
                //Remove the dead block from the alive array
                m_AliveFloorArray.RemoveAt(i);
            }          
        }

        yield return null;

        //Once the dead block have been removed then check to see if we're running out of alive ones!
        if (m_AliveFloorArray.Count < CREATE_NEXT_COMBO_CHECK)
        {
            //Spawn a new combo
            string[] comboArray = GameMetadata.Instance.GetRandomFloorCombo();

            //If we're in the main menu then spawn the main menu floors
            if (GameMetadata.Instance.GetCurrentState() == GameMetadata.eGameStates.MAIN_MENU)
            {
                comboArray = GameMetadata.Instance.GetMainMenuFloorCombo();
            }

            if (comboArray == null)
            {
                yield break;
            }

            curFloor = null;
            
            
            //Go through the combo array and place le floor
            for (int i = 0; i < comboArray.Length; i++)
            {
                curFloor = GetFloorFromType(comboArray[i]);

                if (curFloor != null)
                {
                    curFloorBase = curFloor.GetComponent<FloorBase>();

                    curFloor.SetActive(true);
                    curFloorBase.SetEntryPos(m_CurrentFloorPiece.GetComponent<FloorBase>().GetExitPos());
                    curFloorBase.OnAlive();

                    m_CurrentFloorPiece = curFloor;

                    //Move the floor piece to the alive array
                    m_AliveFloorArray.Add(curFloor);
                    m_DeadFloorArray.Remove(curFloor);

                }
            }
        }

        yield return new WaitForSeconds(0.2f);
    }

    //Spawn blocks into the array. Probably will have to modify to spawn different block types
    void SpawnFloorPieces()
    {
        GameObject newFloor = null;

        for (int x = 0; x < (int)GameMetadata.NUM_OF_TYPES; x++)
        {
            for (int i = 0; i < NUM_OF_EACH_FLOOR_TYPE; i++)
            {                
                switch (x)
                {
                    case 0:
                        newFloor = Instantiate<GameObject>(m_FloorMenu);
                        break;
                    case 1:
                        newFloor = Instantiate<GameObject>(m_FloorPlain);
                        break;
                    case 2:
                        newFloor = Instantiate<GameObject>(m_FloorSwipe);
                        break;
                    case 3:
                        newFloor = Instantiate<GameObject>(m_FloorGap);
                        break;
                }
                                                
                newFloor.transform.position = m_Graveyard.transform.position;
                newFloor.transform.SetParent(m_Graveyard.transform);
                newFloor.SetActive(false);

                m_DeadFloorArray.Add(newFloor);
            }
        }
        
    }


    //Check if a block is dead to move to the graveyard
    bool CheckFloorDead(GameObject curFloor)
    {
        if (curFloor.GetComponent<Renderer>() == null)
        {
            if (curFloor.transform.position.x < m_Player.position.x &&
                !curFloor.GetComponentInChildren<Renderer>().isVisible && curFloor.activeSelf)
            {
                return true;
            }
        }
        else
        {
            if (curFloor.transform.position.x < m_Player.position.x &&
                !curFloor.GetComponent<Renderer>().isVisible && curFloor.activeSelf)
            {
                return true;
            }
        }

        return false;
    }

    //Gets a block of a certain type from the type block array
    GameObject GetFloorFromType(string type)
    {
        for (int i = 0; i < m_DeadFloorArray.Count; i++)
        {
            if (((GameObject)m_DeadFloorArray[i]).tag == type)
            {
                return (GameObject)m_DeadFloorArray[i];
            }
        }

        return null;
    }

    //Call on death to destroy all floors and start again
    public void ClearAllFloors()
    {
        GameObject curFloor;

        int ArrayCount = m_AliveFloorArray.Count;

        for (int i = 0; i < ArrayCount; i++)
        {
            curFloor = (GameObject)m_AliveFloorArray[i];
                        
            //If so add it back to dead array
            curFloor.SetActive(false);
            curFloor.GetComponent<FloorBase>().OnDeath();
            curFloor.transform.position = m_Graveyard.transform.position;

            //At the dead block to the dead block array
            m_DeadFloorArray.Add(curFloor);
            //Remove the dead block from the alive array
           // m_AliveFloorArray.Remove(curFloor);
            
        }

        m_AliveFloorArray.Clear();

        //Move the starting floor back to the start.
        m_StartingFloor.transform.position = new Vector3(m_Player.position.x , m_StartingFloor.transform.position.y, m_StartingFloor.transform.position.z);
        m_CurrentFloorPiece = m_StartingFloor;
    }
}
