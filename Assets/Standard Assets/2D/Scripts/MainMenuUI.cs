﻿using UnityEngine;
using System.Collections;

public class MainMenuUI : MonoBehaviour {

    public GameObject m_StartObj;
    private TextMesh m_StartMesh;

    public LayerMask m_TouchLayerMask;

    // Use this for initialization
    void Awake() {
	    
	}
	
	// Update is called once per frame
	void Update () {
        //Check if the player taps the screen
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 touchPos = Input.mousePosition;

            if (Input.touchCount > 0)
            {
                touchPos = Input.touches[0].position;
            }
            Debug.Log(touchPos);

            Vector3 screenPos = Camera.main.ScreenToWorldPoint(touchPos);

            RaycastHit2D hit = Physics2D.Raycast(screenPos, Vector2.zero);

            if (hit != null && hit.collider != null)
            {
                if (hit.collider.tag == GameMetadata.TAG_STARTBUTTON)
                {
                    ExitState();
                    GameMetadata.Instance.ChangeState(GameMetadata.eGameStates.IN_GAME);
                }
            }            
        }
    }

    public void EnterState()
    {
        m_StartObj.SetActive(true);
    }

    public void ExitState()
    {
        m_StartObj.SetActive(false);
        
    }
    


}
