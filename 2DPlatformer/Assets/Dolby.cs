﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class Dolby : MonoBehaviour
{
    private GameObject debugText;
    public Font arial; 

    [DllImport("DSPlugin")]
    public static extern  bool isAvailable();
    [DllImport("DSPlugin")]
    public static extern  int initialize();
    [DllImport("DSPlugin")]
    public static extern  int setProfile(int profileid);
    [DllImport("DSPlugin")]
    public static extern  int suspendSession();
    [DllImport("DSPlugin")]
    public static extern  int restartSession();
    [DllImport("DSPlugin")]
    public static extern void release();

    // Use this for initialization
    void Start()
    {
        debugText = new GameObject();
        debugText.AddComponent<GUIText>();
        debugText.GetComponent<GUIText>().font = arial;
        debugText.GetComponent<GUIText>().fontSize = 14;
        debugText.GetComponent<GUIText>().color = new Color(255, 0, 0);
        debugText.transform.position = new Vector3(0, 1, 0);
        
        /* Initialize Dolby if Available */

        if (isAvailable())
        {
            Invoke(Init, 0.1f); //Wait a 100ms to make sure Dolby Service is enabled
        }
        else
        {
            debugText.GetComponent<GUIText>().text = "Dolby Sound Not Available";
        }
    }

    void Init()
    {
        debugText.GetComponent<GUIText>().text = "Dolby Sound Available";
        setProfile(2); /* Set Profile to "Game" */
        initialize();
    }

    void OnApplicationPause()
    {
        suspendSession();
    }

    void OnApplicationFocus()
    {
        restartSession();
    }

    void OnApplicationQuit()
    {
        release();
    }
}
