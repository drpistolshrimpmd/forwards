﻿using UnityEngine;
using System.Collections;

public class FloorSwipe : FloorBase {
    
    public GameObject m_BlockSpawnPos;
    public int m_BlockAmount = 4;
    public GameObject m_Block;
    private Vector3 m_BlockExtents;

    private ArrayList m_BlockArray = new ArrayList();
    private int m_CurrentBlockNum = 0;


    // Use this for initialization
    void Awake()
    {
        SetEntryExitPoints();
        CreateObstacle();

        m_BlockExtents = m_Block.GetComponent<SpriteRenderer>().bounds.extents;

        SpawnBlocks();

        //CreateObstacle();
    }

    // Update is called once per frame
    void Update()
    {
        GameObject curBlock;
        BlockSwipe curBlockSwipe;
        //Check if the current block is dead or not to update
        for (int i = 0; i < m_BlockArray.Count; i++)
        {
            curBlock = (GameObject)m_BlockArray[i];
            curBlockSwipe = curBlock.GetComponent<BlockSwipe>();
            //Bad logic to update which block is the next block
            //SHOULD PROBABLY DO LIKE A LIST THING WITH BLOCKS HAVING REFERENCE TO THE NEXT BLOCK
            if (curBlockSwipe.Swiped() && curBlockSwipe.m_CurrentBlock == true)
            {
                m_CurrentBlockNum++;
                curBlockSwipe.m_CurrentBlock = false;

                //Check if there's no more blocks to swipe
                if (m_CurrentBlockNum >= m_BlockArray.Count)
                {

                    m_Completed = true;
                    
                }
            } else if (curBlockSwipe.m_BlockNumber == m_CurrentBlockNum && 
                !curBlockSwipe.Swiped() && curBlockSwipe.m_CurrentBlock == false)
            {
                curBlockSwipe.m_CurrentBlock = true;
            }
        }
    }

    //Spawns the blocks in pile
    protected override void CreateObstacle()
    {
        Vector3 curSpawnPos = m_BlockSpawnPos.transform.position;
        GameObject curBlock;
        BlockSwipe curBlockSwipe;
        for (int i = 0; i < m_BlockArray.Count; i++)
        {
            curBlock = (GameObject)m_BlockArray[i];
            curBlockSwipe = curBlock.GetComponent<BlockSwipe>();
            
            curBlockSwipe.SpawnBlock(i, curSpawnPos);
            
            //Move next block up
            curSpawnPos = new Vector3(curSpawnPos.x, curSpawnPos.y + m_BlockExtents.y * 2f, 0f);
        }

        m_CurrentBlockNum = 0;
    }
    
    private void SpawnBlocks()
    {
        GameObject newBlock;
        for (int i = 0; i < m_BlockAmount; i++)
        {
            newBlock = Instantiate<GameObject>(m_Block);

            newBlock.transform.SetParent(this.transform);
            newBlock.SetActive(false);

            m_BlockArray.Add(newBlock);

        }
    }

    private void KillBlocks()
    {
        GameObject curBlock;
        BlockSwipe curBlockSwipe;

        for (int i = 0; i < m_BlockArray.Count; i++)
        {
            curBlock = (GameObject)m_BlockArray[i];
            curBlockSwipe = curBlock.GetComponent<BlockSwipe>();

            curBlockSwipe.KillBlock();
        }
    }

    public override void OnAlive()
    {
        m_Completed = false;
        CreateObstacle();
    }

    public override void OnDeath()
    {
        KillBlocks();

        m_Completed = true;
    }

    public override bool GetCompleted()
    {
        return m_Completed;
    }


    //Check if someone enters the trigger
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == PlayerBase.m_StaticPlayer.tag)
        {
            PlayerBase.m_StaticPlayer.SlowPlayer();
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == PlayerBase.m_StaticPlayer.tag)
        {
            if (m_Completed)
            {
                PlayerBase.m_StaticPlayer.ResumeNormalSpeed();
            }

        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == PlayerBase.m_StaticPlayer.tag)
        {
            PlayerBase.m_StaticPlayer.ResumeNormalSpeed();
        }
    }
}
