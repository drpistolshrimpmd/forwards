﻿using UnityEngine;
using System.Collections;

public class BlockSwipe : MonoBehaviour {

    //The direction to swipe to clear the block
    private InputDetect.SwipeDirection m_BlockDirection;
    //Am I the block that needs to be swiped
    public int m_BlockNumber { get;  set; }
    public bool m_CurrentBlock { get; set; }

    public GameObject m_TextObj;
    private TextMesh m_DirText;

    private Renderer m_BlockRenderer;

    //If the block has been swiped and completed
    private bool m_Swiped = false;

	// Use this for initialization
	void Awake ()
    {
        m_DirText = m_TextObj.GetComponent<TextMesh>();
        m_BlockRenderer = this.GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (m_CurrentBlock)
        {
            if (InputDetect.Instance.GetCurrentSwipeDir() == m_BlockDirection && m_BlockRenderer.isVisible)
            {
                //Explode spectacularly

                //Then deactivate
                KillBlock();
            }
        }
	}

    public void KillBlock()
    {
        m_BlockNumber = -1;
        m_Swiped = true;
        gameObject.SetActive(false);
    }

    //Reset all necessary things for the block
    public void SpawnBlock(int blocknumber, Vector3 position)
    {
        gameObject.transform.position = position;
        m_BlockNumber = blocknumber;
        m_Swiped = false;
        //m_BlockDirection = InputDetect.SwipeDirection.UP;
        m_BlockDirection = InputDetect.Instance.GetRandomDirection();
        gameObject.SetActive(true);
        SetText();
    }

    public bool Swiped()
    {
        return m_Swiped;
    }

    private void SetText()
    {
        if (m_BlockDirection == InputDetect.SwipeDirection.UP)
        {
            m_DirText.text = "UP";
        }
        else if (m_BlockDirection == InputDetect.SwipeDirection.DOWN)
        {
            m_DirText.text = "DOWN";
        }
        else if (m_BlockDirection == InputDetect.SwipeDirection.LEFT)
        {
            m_DirText.text = "LEFT";
        }
        else if (m_BlockDirection == InputDetect.SwipeDirection.RIGHT)
        {
            m_DirText.text = "RIGHT";
        }
        else
        {
            m_DirText.text = "";
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == GameMetadata.TAG_PLAYER)
        {
            PlayerBase.m_StaticPlayer.KillPlayer();
        }
    }
}
