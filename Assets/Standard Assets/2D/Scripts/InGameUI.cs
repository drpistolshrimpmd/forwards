﻿using UnityEngine;
using System.Collections;

public class InGameUI : MonoBehaviour {

    public GameObject m_ScoreGOb;
    private TextMesh m_ScoreMesh;

    public GameObject m_CoinsGOb;
    private TextMesh m_CoinsMesh;

	// Use this for initialization
	void Awake () {
        m_ScoreMesh = m_ScoreGOb.GetComponent<TextMesh>();
        m_CoinsMesh = m_CoinsGOb.GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
        m_ScoreMesh.text = PlayerBase.m_StaticPlayer.GetScore().ToString();
        m_CoinsMesh.text = "Coins: " + PlayerBase.m_StaticPlayer.GetCoinCount().ToString();
    }

    public void EnterState()
    {
        m_ScoreGOb.SetActive(true);
        m_CoinsGOb.SetActive(true);
    }

    public void ExitState()
    {
        m_ScoreGOb.SetActive(false);
        m_CoinsGOb.SetActive(false);
    }
}
