﻿using UnityEngine;
using System.Collections;

public class PickUpCoin : PickUpBase {

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < PlayerBase.m_StaticPlayer.transform.position.x &&
                !GetComponent<Renderer>().isVisible)
        {
            gameObject.SetActive(false);
            m_Collected = true;
        }
    }

    //For when the player hits
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == GameMetadata.TAG_PLAYER)
        {
            OnCollect();
        }
    }

    protected override void OnCollect()
    {
        PlayerBase.m_StaticPlayer.AddCoins(1);
        gameObject.SetActive(false);
        m_Collected = true;
    }

    public override void OnSpawn()
    {
        m_Collected = false;
    }

    public override void OnDeath()
    {
        
    }
}
