﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerBase : MonoBehaviour {
    //Debug variable for testing
    public bool m_Moving = true;

	public float m_ForwardAcc = 15f;
	public float m_Decel = 0.8f;
	public float m_MaxForwardSpeed = 2.0f;
    private float m_ForwardSpeedModifier = 1.0f;
	public float m_JumpForce = 50.0f;
    public float m_JumpForceMin = 10.0f;
    //How long until the player's jump is fully charged
    public float m_JumpChargeFullTime = 1.0f;

	//Am I on the ground?
	protected bool m_Grounded = false;

    protected float m_JumpChargeTimer = 0.0f;

    protected Animator m_Animator;
    protected Transform m_Transform;
    protected Rigidbody2D m_RigidBody2D;

    //Touch stuff
    protected int m_FingerID;

    public static PlayerBase m_StaticPlayer = null;

    //Score Stuff for the current round
    protected int m_CurrentScore = 0;
    public int m_ScoreDistance = 10;
    protected Vector3 m_ScorePos;
    
    protected int m_CoinCount = 0;

    //The jump charge meter stuff
    public Image m_JumpChargeMeterImg;
    protected float m_JumpChargeMeterHeight;
    


    // Use this for initialization
    void Start () {
		m_Animator = GetComponent<Animator>();
		m_Transform = transform;
		m_RigidBody2D = GetComponent<Rigidbody2D>();

        m_ScorePos = m_Transform.position;

        m_JumpChargeMeterImg.fillAmount = 0;
	}

    void Awake()
    {
        if (m_StaticPlayer == null)
        {
            m_StaticPlayer = this;
        }
    }


	void FixedUpdate () {

		if (m_RigidBody2D.velocity.y > -Mathf.Epsilon && m_RigidBody2D.velocity.y < Mathf.Epsilon)
		{
			m_Grounded = true;
		}

		int horizontal = 1;
		int vertical = 0;
        

		//Taps and swipes
        if (InputDetect.Instance.GetCurrentSwipeDir() == InputDetect.SwipeDirection.LONG_PRESS && 
            GameMetadata.Instance.GetCurrentState() == GameMetadata.eGameStates.IN_GAME)
        {
            vertical = 1;
            
        }
        else
        {
            vertical = 0;

        }

        

		if (m_Moving)
		{
			//Transition to moving animation
			m_Animator.SetBool("Moving", true);

			//Accelerate the player and then clamp it to the maximum
			float hVel = m_RigidBody2D.velocity.x + (horizontal * m_ForwardAcc * Time.deltaTime);
			Mathf.Clamp(hVel, -m_MaxForwardSpeed, m_MaxForwardSpeed);
			
			//m_RigidBody2D.velocity = new Vector2(hVel, m_RigidBody2D.velocity.y);

			m_RigidBody2D.velocity = new Vector2(horizontal * (m_MaxForwardSpeed * m_ForwardSpeedModifier), m_RigidBody2D.velocity.y);
		}
		else
		{
			//Transition out of moving animation
			m_Animator.SetBool("Moving", false);

			if (Mathf.Abs(m_RigidBody2D.velocity.x) > Mathf.Epsilon && m_Grounded)
			{
				//Slow down faster
				m_RigidBody2D.velocity = new Vector2(m_RigidBody2D.velocity.x * (m_Decel), m_RigidBody2D.velocity.y); 
			}

		}

        //If pressed jump button
        if (vertical > 0)
        {
            m_JumpChargeTimer += Time.deltaTime;

            //Clamp the jump charge timer to the full charge time
            Mathf.Clamp(m_JumpChargeTimer, 0f, m_JumpChargeFullTime);

        }
        else
        {
            //if no vertical press then check if the jump charge timer was started or not
            //Also check to see if not swiped. If swiped then the jump is cancelled
            if (m_JumpChargeTimer > 0.0f && InputDetect.Instance.GetCurrentSwipeDir() == InputDetect.SwipeDirection.TAP)
            {
                
                //TAP was held for a little bit so then jump
                m_RigidBody2D.AddForce(new Vector2(0f,  
                    Mathf.Clamp(m_JumpForce * (m_JumpChargeTimer / m_JumpChargeFullTime), m_JumpForceMin, m_JumpForce)));

                //Reset the jump charge timer
                m_JumpChargeTimer = 0.0f;
            }
            else
            {
                m_JumpChargeTimer = 0.0f;
            }
        }

        //Update the fill of the jump charge bar
        m_JumpChargeMeterImg.fillAmount = m_JumpChargeTimer / m_JumpChargeFullTime;

        CheckDead();
        CheckScore();
	}

    void CheckDead()
    {
        if (m_Transform.position.y < -20f)
        {           
            //Adjust score
            KillPlayer();

        }

    }

    //Calculates if the player has travelled enough for a point
    void CheckScore()
    {
        if (m_Transform.position.x - m_ScorePos.x >= m_ScoreDistance && GameMetadata.Instance.GetCurrentState() == GameMetadata.eGameStates.IN_GAME)
        {
            AddToScore(1);
            m_ScorePos = m_Transform.position;
        }
    }
    
    //Slows player speed. Largely for swipe obstacle
    public void SlowPlayer()
    {
        m_ForwardSpeedModifier = 0.5f;
    }

    public void ResumeNormalSpeed()
    {
        m_ForwardSpeedModifier = 1.0f;

    }

    //Fall death. Stops forward momentum
    public void FallDeath()
    {
        
        
    }

    public void AddToScore (int amount)
    {
        m_CurrentScore += amount;
    }

    public void KillPlayer()
    {
        m_CurrentScore = 0;

        //Reset the player
        m_Transform.position = new Vector3(m_Transform.position.x, 3f, m_Transform.position.z);
        ResumeNormalSpeed();

        GameMetadata.Instance.ChangeState(GameMetadata.eGameStates.MAIN_MENU);
    }
    
    public int GetScore()
    {
        return m_CurrentScore;
    }
 
    public void AddCoins(int numCoins)
    {
        m_CoinCount += numCoins;
    }

    public int GetCoinCount()
    {
        return m_CoinCount;
    }
       
}
