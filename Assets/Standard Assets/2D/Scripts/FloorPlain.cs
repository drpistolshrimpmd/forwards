﻿using UnityEngine;
using System.Collections;

public class FloorPlain : FloorBase {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    protected override void CreateObstacle()
    {

        base.CreateObstacle();
    }

    public override void OnAlive()
    {
        if (Random.Range(0, 10) > 2)
        {
            CoinManager.m_Manager.SpawnCoin(m_CoinSpawnPos.transform.position);
        }
        else
        {
            CoinManager.m_Manager.SpawnCoinSlab(m_CoinSpawnPos.transform.position);
        }
    }

    public override void OnDeath()
    {
        base.OnDeath();
    }


}
