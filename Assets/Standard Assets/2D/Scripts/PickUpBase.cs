﻿using UnityEngine;
using System.Collections;

public class PickUpBase : MonoBehaviour {

    protected bool m_Collected = false;

	// Use this for initialization
	void Start () {
	
	}

    protected virtual void OnCollect() { }
    public virtual void OnSpawn() { }
    public virtual void OnDeath() { }

    public bool IsCollected()
    {
        return m_Collected;
    }

}
