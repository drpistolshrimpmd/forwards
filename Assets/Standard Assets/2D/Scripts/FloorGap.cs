﻿using UnityEngine;
using System.Collections;

public class FloorGap : FloorBase {

    public GameObject m_RightPiece;
    public GameObject m_LeftPiece;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    protected override void CreateObstacle()
    {
        //Have a chance of the right piece being higher or lower than the left to give some verticality
        if (Random.Range(0, 10) > 3)
        {
            float newYPos = 0.0f;

            if (Random.Range(0,2) == 0)
            {
                //Go up
                newYPos = m_RightPiece.transform.position.y + Random.Range(1.0f, 3.0f);
            }
            else
            {
                //Go down
                newYPos = m_RightPiece.transform.position.y - Random.Range(1.0f, 5.0f);
            }

            //Don't let the level go too far up or down
            Mathf.Clamp(newYPos, -17f, 0f);

            m_RightPiece.transform.position = new Vector3(m_RightPiece.transform.position.x,
                    newYPos, 0.0f);
        }
    }


    //Check if someone enters the trigger and keeeeell them
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == PlayerBase.m_StaticPlayer.tag)
        {
            PlayerBase.m_StaticPlayer.FallDeath();
        }
    }

    public override void OnAlive()
    {
        CreateObstacle();
    }

    public override void OnDeath()
    {
        //Reset the right piece to be the same height as the left
        m_RightPiece.transform.position = new Vector3(m_RightPiece.transform.position.x,
                       m_LeftPiece.transform.position.y, 0.0f);
    }
}
