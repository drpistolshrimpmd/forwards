﻿using UnityEngine;
using System.Collections;

public class InputDetect : MonoBehaviour {

    public static InputDetect Instance { get; private set; }

	//To determine if a swipe is a swipe
	public float m_MinSwipeVelocity = 100f;
	public float m_MinSwipeDist = 100;

	private Vector2 m_StartPos = Vector2.zero;
	private float m_SwipeStartTime = -1f;

	private SwipeDirection m_CurrentSwipeDir = SwipeDirection.NONE;
	public enum SwipeDirection
	{
		NONE = 0,
		UP,
		DOWN,
		LEFT,
		RIGHT,
        TAP,
        LONG_PRESS,
        NUM_DIRECTIONS
	};

	// Use this for initialization
	void Awake () {
        //Make sure there already isn't an instance
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        //Create the instance
        Instance = this;

        //Don't destroy between scenes
        DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {

        //If the mouse isn't down then clear the input
        if (!Input.GetMouseButton(0) && m_SwipeStartTime < 0f)
        {
            m_CurrentSwipeDir = SwipeDirection.NONE;
        }

        if (Input.GetMouseButtonDown(0)) 
		{
            //New press
            m_StartPos = new Vector2(Input.mousePosition.x,
                Input.mousePosition.y);

            if (Input.touchCount > 0)
            {
                m_StartPos = Input.touches[0].position;
            }

            m_SwipeStartTime = Time.time;
            
		}

        if (Input.GetMouseButton(0) || Input.touchCount > 0)
        {
            //Press has started so check if it's a long tap
            Vector2 curPos = new Vector2(Input.mousePosition.x,
                                 Input.mousePosition.y);

            if (Input.touchCount > 0)
            {
                curPos = Input.touches[0].position;
            }

            Vector2 swipeVector = curPos - m_StartPos;

            float deltaTime = Time.time - m_SwipeStartTime;
            float velocity = swipeVector.magnitude / deltaTime;

            //Check if the player hasn't moved their finger much and have a little time delay so it doesn't
            //think tap at the start of a swipe.
            if (swipeVector.magnitude < 100f && (deltaTime) > .01f && velocity < m_MinSwipeVelocity)
            {
                m_CurrentSwipeDir = SwipeDirection.LONG_PRESS;
            }
        }

        if (Input.GetMouseButtonUp (0)) 
		{
			float deltaTime = Time.time - m_SwipeStartTime;

			Vector2 endPos = new Vector2 (Input.mousePosition.x,
				                 Input.mousePosition.y);
			Vector2 swipeVector = endPos - m_StartPos;

			float velocity = swipeVector.magnitude / deltaTime;

			if (velocity > m_MinSwipeVelocity &&
			    swipeVector.magnitude > m_MinSwipeDist
                && m_CurrentSwipeDir != SwipeDirection.TAP) {
				//Swipe has happened

				swipeVector.Normalize ();

                //Check if up down left or right
                if (swipeVector.y > 0 && swipeVector.x > -0.5f && swipeVector.x < 0.5f)
                {
                   // Debug.Log("Up swipe");
                    m_CurrentSwipeDir = SwipeDirection.UP;
                }
                else if (swipeVector.y < 0 && swipeVector.x > -0.5f && swipeVector.x < 0.5f)
                {
                   // Debug.Log("Down swipe");
                    m_CurrentSwipeDir = SwipeDirection.DOWN;
                }
                else if (swipeVector.x > 0 && swipeVector.y > -0.5f && swipeVector.y < 0.5f)
                {
                   // Debug.Log("Right swipe");
                    m_CurrentSwipeDir = SwipeDirection.RIGHT;
                }
                else if (swipeVector.x < 0 && swipeVector.y > -0.5f && swipeVector.y < 0.5f)
                {
                   // Debug.Log("Left swipe");
                    m_CurrentSwipeDir = SwipeDirection.LEFT;
                }

                
            }
			else 
			{
                //It's a tap!
               // Debug.Log("TAP!");
                m_CurrentSwipeDir = SwipeDirection.TAP;
			}

            m_SwipeStartTime = -1f;

        }
	}

	public SwipeDirection GetCurrentSwipeDir()
	{
		return m_CurrentSwipeDir;

	}

    public SwipeDirection GetRandomDirection()
    {
        int rand = (int)SwipeDirection.NONE;

        //Just to make sure NONE is not returned
        while (rand == (int)SwipeDirection.NONE || rand == (int)SwipeDirection.TAP || rand == (int)SwipeDirection.LONG_PRESS)
        {
            rand = Random.Range(0, (int)SwipeDirection.NUM_DIRECTIONS);
        }

        return (SwipeDirection)rand;
    }
}
