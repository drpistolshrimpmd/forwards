﻿using UnityEngine;
using System.Collections;

public class CoinManager : ObjectManagerBase
{
    public static CoinManager m_Manager = null;

    public GameObject[] m_CoinSlabs;
    public ArrayList m_SlabArray = new ArrayList();

    // Use this for initialization
    void Start()
    {
        CreateObjects();

        if (m_Manager == null)
        {
            m_Manager = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateObjects();
    }

    protected override void CreateObjects()
    {
        base.CreateObjects();

        
    }

    protected override void UpdateObjects()
    {
        GameObject curObj = null;

        for (int i = 0; i < m_AliveArray.Count; i++)
        {
            curObj = (GameObject)m_AliveArray[i];

            //If object is collected then move to dead array
            if (curObj.GetComponent<PickUpCoin>().IsCollected())
            {
                m_DeadArray.Add(curObj);
                m_AliveArray.Remove(curObj);
            }
        }
    }

    public override void KillAllObjects()
    {
        base.KillAllObjects();

        GameObject curSlab = null;

        for (int i = 0; i < m_SlabArray.Count; i++)
        {
            curSlab = (GameObject)m_SlabArray[i];
            //m_SlabArray.Remove(curSlab);
            Destroy(curSlab);    
        }
    }

    public void SpawnCoin(Vector3 spawnPos)
    {
        if (m_DeadArray.Count > 0)
        {
            GameObject newCoin = (GameObject)m_DeadArray[0];

            newCoin.SetActive(true);
            newCoin.transform.position = spawnPos;
            newCoin.GetComponent<PickUpCoin>().OnSpawn();

            //Put it in the alive array
            m_AliveArray.Add(newCoin);
            m_DeadArray.Remove(newCoin);
        }
    } 

    public void SpawnCoinSlab(Vector3 spawnPos)
    {
        GameObject newCoinSlab = Instantiate<GameObject>(m_CoinSlabs[0]);

        newCoinSlab.SetActive(true);
        newCoinSlab.transform.position = spawnPos;

        m_SlabArray.Add(newCoinSlab);
    }
}
