﻿using UnityEngine;
using System.Collections;

//Base class that all ground objects will use.
public class FloorBase : MonoBehaviour {
    
    public GameObject m_EntryPos;
    public GameObject m_ExitPos;
    public GameObject m_CoinSpawnPos;

    protected Vector3 m_Extents;
    protected SpriteRenderer m_SpriteRenderer;

    protected bool m_Completed = false;

	// Use this for initialization
	void Start () {
        
    }

    void Awake()
    {
        CreateObstacle();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    protected virtual void CreateObstacle()
    {
        SetEntryExitPoints();
    }

    public virtual void OnAlive()
    {
        
    }

    public virtual void OnDeath()
    {
       
    }

    public virtual bool GetCompleted()
    {
        return true;
    }

    protected virtual void SetEntryExitPoints()
    {
        //Create entry and exit gameobjects. Game objects used to make it easier to see in debug.
        m_EntryPos = new GameObject("Entry Pos");
        m_EntryPos.transform.parent = transform;

        m_ExitPos = new GameObject("Exit Pos");
        m_ExitPos.transform.parent = transform;

        m_SpriteRenderer = this.GetComponent<SpriteRenderer>();
        m_Extents = m_SpriteRenderer.bounds.extents;

        //Place the extent gameobjects so the pieces can be joined.
        m_EntryPos.transform.position = this.transform.position - new Vector3(m_Extents.x, -m_Extents.y, m_Extents.z);

        m_ExitPos.transform.position = this.transform.position + m_Extents;
    }

    public Vector3 GetExitPos()
    {
        return m_ExitPos.transform.position;
    }

    public Vector3 GetEntryPos()
    {
        return m_EntryPos.transform.position;
    }

    //Move the block so that the entry pos is at this position.
    public void SetEntryPos(Vector3 newEntryPos)
    {
        this.transform.position = newEntryPos + (this.transform.position - m_EntryPos.transform.position);
    }
    
}
