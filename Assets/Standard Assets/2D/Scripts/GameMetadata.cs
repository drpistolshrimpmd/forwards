﻿using UnityEngine;
using System.Collections;

public class GameMetadata : MonoBehaviour {

    public static GameMetadata Instance { get; private set; }

    #region INITIATION REGION

    void Awake()
    {
        //Check if there's an instance and it's not this one
        if (Instance != null && Instance != this)
        {
            //Kill it with fire
            Destroy(gameObject);
        }

        Instance = this;

        //Keep this bad boy around
        DontDestroyOnLoad(gameObject);

        CreateFloorCombos();
    }

    #endregion

    #region OVERALL GAME DATA

    public const string TAG_PLAYER = "Player";
    public const string TAG_STARTBUTTON = "START_BUTTON";

    public const string MAINMENUUIMASK = "MainMenuUI";

    private int m_TotalCoins = 0;
    private int m_HighScore = 0;

    //Checks if got a new high score. Returns true if new best
    public bool CheckHighScore(int newScore)
    {
        if (newScore > m_HighScore)
        {
            m_HighScore = newScore;
            return true;
        }

        return false;
    }

    public int GetHighScore()
    {
        return m_HighScore;
    }

    public void AddCoins(int newCoins)
    {
        m_TotalCoins += newCoins;
    }

    public int GetTotalCoins()
    {
        return m_TotalCoins;
    }

    #endregion

    #region GAME STATE DATA

    public enum eGameStates
    {
        MAIN_MENU = 0,
        IN_GAME,
        PAUSED
    };

    private eGameStates m_CurrentState = eGameStates.MAIN_MENU;

    public eGameStates GetCurrentState()
    {
        return m_CurrentState;
    }

    public void ChangeState(eGameStates newState)
    {
        m_CurrentState = newState;

        if (m_CurrentState == eGameStates.IN_GAME)
        {
            Camera.main.GetComponentInChildren<InGameUI>().EnterState();
        }
        else if (m_CurrentState == eGameStates.MAIN_MENU)
        {
            Camera.main.GetComponentInChildren<InGameUI>().ExitState();
            Camera.main.GetComponentInChildren<MainMenuUI>().EnterState();

            Camera.main.GetComponentInChildren<LevelGenerator>().ClearAllFloors();
            CoinManager.m_Manager.KillAllObjects();
        }
    }

    #endregion

    #region MAIN LOOP

    void Update()
    {
        switch (m_CurrentState)
        {
            case eGameStates.MAIN_MENU:
                {

                    break;
                }
            case eGameStates.IN_GAME:
                {

                    break;
                }
            case eGameStates.PAUSED:
                {

                    break;
                }
        }
    }

    #endregion

    #region LEVEL GENERATION DATA

    const int NUM_OF_COMBOS = 5;

    public const string FLOORMENUTAG = "FLOORMENU";
    public const string FLOORPLAINTAG = "FLOORPLAIN";
    public const string FLOORSWIPETAG = "FLOORSWIPE";
    public const string FLOORGAPTAG = "FLOORGAP";

    //MAKE SURE TO UPDATE THIS ERRYTIME YOU ADD A NEW FLOOR TYPE
    public const int NUM_OF_TYPES = 4;

    public string[][] m_FloorComboArray = new string[NUM_OF_COMBOS][];

    //Hard code the floor combo types
    void CreateFloorCombos()
    {
        m_FloorComboArray[0] = new string[] { FLOORMENUTAG };
        m_FloorComboArray[1] = new string[] { FLOORPLAINTAG };
        m_FloorComboArray[2] = new string[]{ FLOORPLAINTAG, FLOORSWIPETAG };
        m_FloorComboArray[3] = new string[] { FLOORPLAINTAG, FLOORGAPTAG };
    }


    public string[] GetFloorCombo (int comboToGet)
    {
        return m_FloorComboArray[comboToGet];
    }

    public string[] GetRandomFloorCombo ()
    {
        return m_FloorComboArray[Random.Range(0, m_FloorComboArray.Length - 1)];        
    }

    public string[] GetMainMenuFloorCombo()
    {
        return m_FloorComboArray[0];
    }

    #endregion
}
