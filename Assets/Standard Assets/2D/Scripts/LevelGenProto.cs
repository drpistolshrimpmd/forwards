﻿using UnityEngine;
using System.Collections;


public class LevelGenProto : MonoBehaviour {

	public GameObject m_Block;
    public GameObject m_ObstacleSwipe;
	public int m_BlockCount = 15;
	public GameObject m_Graveyard;

	private SpriteRenderer m_BlockSprite;
	private Vector3 m_BlockExtents;

    private Vector3 m_SwipeObsBlockExtents;

	private ArrayList m_BlocksArray = new ArrayList();

	private Transform m_Player;
	private Vector3 m_CurrentPosition;

    private bool m_Gap = false;
    private bool m_ObstacleSwipeActive = false;

	// Use this for initialization
	void Start () {
        //Create the start floor
        m_BlockSprite = m_Block.GetComponent<SpriteRenderer>();
        m_BlockExtents = m_BlockSprite.bounds.extents;

        m_SwipeObsBlockExtents = m_ObstacleSwipe.GetComponentInChildren<SpriteRenderer>().bounds.extents;

        m_CurrentPosition = new Vector3(-10, -7, 0);

		m_Player = GameObject.FindGameObjectWithTag("Player").transform;
		SpawnBlocks();

        
	}
	
	// Update is called once per frame
	void Update () {
		StartCoroutine(UpdateFloor());

        if (m_ObstacleSwipeActive)
        {
            if (CheckBlockDead(m_ObstacleSwipe))
            {
                //If so add it back to dead array
                m_ObstacleSwipe.SetActive(false);
                m_ObstacleSwipe.transform.position = m_Graveyard.transform.position;
                m_ObstacleSwipeActive = false;
            }

        }
	}

	IEnumerator UpdateFloor()
	{
		GameObject curBlock;
		//Check for dead blocks
		for (int i = 0; i < m_BlocksArray.Count; i++)
		{
			curBlock = (GameObject)m_BlocksArray[i];

            //Check if block is behind the character and off screen
            if (CheckBlockDead(curBlock))
			{
				//If so add it back to dead array
				curBlock.SetActive(false);
				curBlock.transform.position = m_Graveyard.transform.position;
			}
			else if (curBlock.activeSelf == false)
			{
				//Debug.Log("Activate block");
				//Place the new block at the start
				
                float extentMulti = 2f;

                int rand = Random.Range(0, 15);
                if (rand < 3 && m_Gap == false)
                {
                    extentMulti = 4f;
                    m_Gap = true;
                }
                else if (rand == 4 && m_ObstacleSwipeActive == false)
                {
                    //Make it a swipe obstacle
                    curBlock = m_ObstacleSwipe;
                    curBlock.GetComponent<FloorSwipe>().OnAlive();
                    m_ObstacleSwipeActive = true;
                }
                else
                {
                    m_Gap = false;
                }
                curBlock.SetActive(true);

                //if (!m_ObstacleSwipeActive)
                //{
                    curBlock.transform.position = new Vector3(m_CurrentPosition.x + (m_BlockExtents.x * extentMulti), m_CurrentPosition.y, 0.0f);

                //}
                //else if (m_ObstacleSwipeActive)
                //{
                //    curBlock.transform.position = new Vector3(m_CurrentPosition.x - (m_BlockExtents.x * 0.5f) + (m_SwipeObsBlockExtents.x * extentMulti), m_CurrentPosition.y, 0.0f);
                //}

                m_CurrentPosition = curBlock.transform.position;
                
			}

			yield return null;
		}
		yield return new WaitForSeconds(0.2f);
	}

	//IEnumerator CreateFloor()
	//{
	//	Vector3 currentPos = new Vector3(-10, -7, 0);
	//	Vector3 blockExtents = m_BlockSprite.bounds.extents;
	//	GameObject newBlock;

	//	for (int i = 0; i < 500; i++)
	//	{
	//		newBlock = Instantiate<GameObject>(m_Block);
	//		newBlock.transform.position = new Vector3(currentPos.x + (blockExtents.x * 2f), currentPos.y, 0.0f);
	//		currentPos = newBlock.transform.position;

	//		yield return new WaitForSeconds(0.01f);
	//	}

        
	//}

	void SpawnBlocks()
	{
		GameObject newBlock;
		for (int i = 0; i < m_BlockCount; i++)
		{

			newBlock = Instantiate<GameObject>(m_Block);
			newBlock.transform.position = m_Graveyard.transform.position;
            newBlock.transform.SetParent(m_Graveyard.transform);
			newBlock.SetActive(false);

			m_BlocksArray.Add(newBlock);

		}


        m_ObstacleSwipe = Instantiate<GameObject>(m_ObstacleSwipe);
        m_ObstacleSwipe.transform.position = m_Graveyard.transform.position;
        m_ObstacleSwipe.transform.SetParent(m_Graveyard.transform);
        m_ObstacleSwipe.SetActive(false);

    }

    bool CheckBlockDead(GameObject curBlock)
    {
        if (curBlock.GetComponent<Renderer>() == null)
        {
            if (curBlock.transform.position.x < m_Player.position.x &&
                !curBlock.GetComponentInChildren<Renderer>().isVisible && curBlock.activeSelf)
            {                
                return true;
            }
        }
        else
        {
            if (curBlock.transform.position.x < m_Player.position.x &&
                !curBlock.GetComponent<Renderer>().isVisible && curBlock.activeSelf)
            {          
                return true;
            }
        }
        

        return false;
    }

}
